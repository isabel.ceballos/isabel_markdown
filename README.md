# Hoja03_Markdown_02 GIT

1. Crea un repositorio en GitLab o GitHub llamado TUNOMBRE_markdown

![Captura1](img/captura1.jpg "Captura 1")
![Captura2](img/captura2.jpg "Captura 2")

2. Pasos para realizar el commit de lo que tengo hasta el momento.
	* git status
	* git add .
	* ![Captura3](img/captura3.jpg "Captura 3")
	* git commit -m "Primer commit de ISABEL"
	* git push

3. Creo el archivo .gitignore poniendo touch .gitignore en el cmd y le pongo dentro las directivas.
	* ![Captura4](img/captura4.jpg "Captura 4") 
4. Creo la tag v0.1.
	* ![Captura5](img/captura5.jpg "Captura 5")


# Hoja03_Markdown_03 GIT	

1. Creación de ramas:
	* Creo la rama ISABEL y me posiciono en ella: git checkout -b ISABEL.
	* ![Captura6](img/captura6.jpg "Captura 6")

2. Añadir un fichero y crear la rama remota:
	* touch despliegue.md
	* Hago el commit
	* ![Captura7](img/captura7.jpg "Captura 7")
	* gut push origin ISABEL
3. Haz un merge directo
	* Me posiciono en master: git checkout master. (:wq) Si pongo un commit sin mensaje me sale otra pantalla, escribo el mensaje y le doy a esc y tecleo :wq.
	Para posicionarme en master de nuevo tengo que hacer add y commit de los cambios que haya hecho mientras estaba en la rama.
	* Hago git merge ISABEL en la rama master.

4. Hago el merge con conflicto y lo soluciono directamente en el archivo que genera el conflicto.

* ![Captura8](img/captura8.jpg "Captura 8")

6. Creo la tag y borro la rama ISABEL
	* git tag v0.2
	* git branch -d ISABEL
	* ![Captura9](img/captura9.jpg "Captura 9")
7. Documento todo y lo subo. 


